const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');

const Products = require('../models/Products');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    if (req.query.categories) {
        Products.find({categories: req.query.categories}).populate('categories')
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    } else {
        Products.find()
            .then(products => res.send(products))
            .catch(error => res.status(400).send(error))
    }
});

router.post('/', auth, upload.single('image'), (req, res) => {
    const productData = req.body;

    if (req.file) {
        productData.image = req.file.filename;
    }

    const products = new Products(productData);

    products.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

module.exports = router;