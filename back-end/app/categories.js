const express = require('express');

const Categories = require('../models/Categories');

const router = express.Router();

router.get('/', (req, res) => {
    Categories.find()
        .then(categories => res.send(categories))
        .catch(() => res.sendStatus(500));
});


router.post('/', (req, res) => {
    const category = new Categories(req.body);

    category.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

module.exports = router;