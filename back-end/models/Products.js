const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProductsSchema = new Schema({
    title: {
        type: String, required: true
    },
    price: {
        type: Number, required: true
    },
    description: String,
    image: String,
    categories: {
        type: Schema.Types.ObjectId,
        ref: 'Categories',
        required: true
    }
});

const Products = mongoose.model('Products', ProductsSchema);

module.exports = Products;