const mongoose = require('mongoose');
const config = require('./config');

const Categories = require('./models/Categories');
const Products = require('./models/Products');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const categories = await Categories.create(
        {title: 'PCs', description: 'Personal Computers'},
        {title: 'Mobile Phones', description: 'SmartPhones'}
    );

    await Products.create(
        {
            title: 'Acer',
            price: 450,
            description: 'Long Battery Life, Numeric Keypad',
            categories: categories[0]._id,
            image: 'Acer.png'
        },
        {
            title: 'Redmi 7 Note',
            price: 300,
            description: '',
            categories: categories[1]._id,
            image: 'redmi_note_7.jpg'
        }
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});