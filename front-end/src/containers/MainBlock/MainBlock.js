import React, {Component, Fragment} from 'react';
import Categories from "../../components/Categories/Categories";
import Products from "../../components/Products/Products";
import ProductCategory from "../../components/ProductCategory/ProductCategory";

import './MainBlock.css'

class MainBlock extends Component {
    state={
        categoryChosed: false
    };

    choseCategory = () => {
        this.setState({categoryChosed: true})
    };
    unChooseCategory = () => {
        this.setState({categoryChosed: false})
    };

    render() {
        let products;

        if (!this.state.categoryChosed) {
            products = <Products/>;
        } else {
            products = <ProductCategory/>;
        }

        return (
            <Fragment>
                <div className="main-block">
                    <Categories
                        unChoose={this.unChooseCategory}
                        choose={this.choseCategory}
                    />
                    {products}
                </div>
            </Fragment>

        );
    }
}

export default MainBlock;