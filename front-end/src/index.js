import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import {createBrowserHistory} from 'history';
import * as serviceWorker from './serviceWorker';
import productsReducer from './store/reducers/ProductsReducer'
import UsersReducer from './store/reducers/UsersReducer'
import {ConnectedRouter, connectRouter, routerMiddleware} from "connected-react-router";



const saveToLocalStorage = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    }catch (e) {
        console.log('Could not save state')
    }
};

const loadFromLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null){
            return undefined;
        }

        return JSON.parse(serializedState)
    }catch (e) {
        return undefined
    }
};

const persistedState = loadFromLocalStorage();

const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    shop: productsReducer,
    user: UsersReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));


const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveToLocalStorage({
        user: {
            user: store.getState().user.user
        }
    });
});


const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
serviceWorker.unregister();
