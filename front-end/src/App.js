import React, {Component, Fragment} from 'react';
import './App.css';
import {Route, Switch, withRouter} from "react-router";
import MainBlock from "./containers/MainBlock/MainBlock";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {logoutUser} from "./store/actions/UserActions";
import {connect} from "react-redux";
import {NotificationContainer} from "react-notifications";
import NewProduct from "./containers/NewProduct/NewProduct";

class App extends Component {
  render() {
    return (
      <Fragment>
          <NotificationContainer/>
          <header>
              <Toolbar user={this.props.user}
                       logout={this.props.logoutUser}/>
          </header>
          <Switch>
              <Route path="/" exact component={MainBlock} />
              <Route path="/register" exact component={Register} />
              <Route path="/login" exact component={Login} />
              <Route path="/new/product" exact component={NewProduct} />
          </Switch>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    user: state.user.user
});
const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
