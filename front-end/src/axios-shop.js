import axios from 'axios'
import {urlShop} from "./constants";

const istance = axios.create({
    baseURL: urlShop
});

export default istance;