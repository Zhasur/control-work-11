import React, {Component} from 'react';

import './Categories.css'
import {fetchSuccess} from "../../store/actions/CategoryActions";
import {connect} from "react-redux";
import {Button} from "reactstrap";
import {fetchProduct} from "../../store/actions/ProductsActions";

class Categories extends Component {
    componentDidMount() {
        this.props.fetchCategories();
    }

    onUnchoose = () => {
        this.props.unChoose()
    };

    onChoose = (id) => {
        this.props.choose();
        this.props.fetchProduct(id);
    };

    render() {
        let categories;
        if (this.props.categories) {
            categories = this.props.categories.map(category => {
                return (
                    <Button className="category-btn" onClick={() => this.onChoose(category._id)} key={category._id}>{category.title}</Button>
                )
            })
        }

        return (
            <div className="categories">
                <Button onClick={this.onUnchoose} >All</Button>
                {categories}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.shop.categories
});

const mapDispatchToProps = dispatch => ({
    fetchCategories: () => dispatch(fetchSuccess()),
    fetchProduct: (id) => dispatch(fetchProduct(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Categories);