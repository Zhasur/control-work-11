import React, {Component} from 'react';
import {connect} from "react-redux";
import {Card, CardBody, CardImg, CardTitle} from "reactstrap";

class ProductCategory extends Component {
    render() {
        let productCat;
        let categoryName;

        if (this.props.productCat) {
            categoryName = this.props.productCat.map(name => {
               return name.categories.title
            });

            productCat = this.props.productCat.map(category => {

                return (
                    <Card className="product-card" key={category._id}>
                        <CardImg
                            style={{width: '100%',  height: '140px'}}
                            src={'http://localhost:8088/uploads/' + category.image}
                            alt="image"
                        />
                        <CardBody className="product-inner">
                            <CardTitle>{category.title}</CardTitle>
                            <CardTitle>{category.price} USD</CardTitle>
                        </CardBody>
                    </Card>
                )
            });
        }

        return (
            <div>
                <h2>{categoryName}</h2>
                {productCat}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    productCat: state.shop.productCat
});

export default connect(mapStateToProps, null)(ProductCategory);