import React, {Component} from 'react';
import {connect} from 'react-redux'

import './Products.css'
import {fetchProducts} from "../../store/actions/ProductsActions";
import {Card, CardBody, CardImg, CardTitle} from "reactstrap";

class Products extends Component {
    componentDidMount() {
        this.props.fetchProducts();
    }

    render() {
        let products;
        if (this.props.products) {
            products = this.props.products.map(product => {
                return (
                    <Card className="product-card" key={product._id}>
                        <CardImg
                            style={{width: '100%',  height: '140px'}}
                            src={'http://localhost:8088/uploads/' + product.image}
                            alt="image"
                        />
                        <CardBody className="product-inner">
                            <CardTitle>{product.title}</CardTitle>
                            <CardTitle>{product.price} USD</CardTitle>
                        </CardBody>
                    </Card>
                )
            });
        }

        return (
            <div className="products">
                <h2>All items</h2>
                <div className="product">
                    {products}
                </div>
            </div>

        );
    }
}

const mapStateToProps = state => ({
    products: state.shop.products
});

const mapDispatchToProps = dispatch => ({
    fetchProducts: () => dispatch(fetchProducts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);