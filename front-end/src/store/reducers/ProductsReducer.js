import {FETCH_PRODUCTCAT_SUCCESS, FETCH_PRODUCTS_SUCCESS} from "../actions/ProductsActions";
import {FETCH_CATEGORIES_SUCCESS} from "../actions/CategoryActions";

const initialState = {
    products: null,
    categories: null,
    productCat: null
};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, products: action.products};
        case FETCH_CATEGORIES_SUCCESS:
            return{...state, categories: action.categories};
        case FETCH_PRODUCTCAT_SUCCESS:
            return {...state, productCat: action.productCat};
        default:
            return state;
    }
};

export default productsReducer;