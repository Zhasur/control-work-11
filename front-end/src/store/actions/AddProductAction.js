import axios from '../../axios-shop';
import {push} from 'connected-react-router';

export const ADD_PRODUCT_SUCCESS = 'ADD_PRODUCT_SUCCESS';

const createProductsSuccess = () => ({type: ADD_PRODUCT_SUCCESS});

export const createProduct = product => {
    return (dispatch, getState) => {
        const token = getState().user.user.token;
        const config = {headers: {"Authorization": token}};
        return axios.post('/products', product, config).then(
            response => {
                dispatch(createProductsSuccess());
                dispatch(push('/'))
            }
        )
    };
};