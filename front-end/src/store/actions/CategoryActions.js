import axios from '../../axios-shop'

export const FETCH_CATEGORIES_SUCCESS = "FETCH_CATEGORIES_SUCCESS";
export const FETCH_CATEGORIES_ERROR = "FETCH_CATEGORIES_ERROR";

const fetchCategoriesSuccess = (categories) => ({type: FETCH_CATEGORIES_SUCCESS, categories});
const fetchCategoriesError = () => ({type: FETCH_CATEGORIES_ERROR});


export const fetchSuccess = () => {
    return dispatch => {
        return axios.get('/categories').then(response => {
            dispatch(fetchCategoriesSuccess(response.data))
        })
        .catch(error => {
            dispatch(fetchCategoriesError(error))
        })
    }
};

