import axios from '../../axios-shop'

export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const FETCH_PRODUCTCAT_SUCCESS = "FETCH_PRODUCTCAT_SUCCESS";
export const FETCH_PRODUCTS_ERROR = "FETCH_PRODUCTS_ERROR";

const fetchProductsSuccess = (products) => ({type: FETCH_PRODUCTS_SUCCESS, products});
const fetchProductsError = () => ({type: FETCH_PRODUCTS_ERROR});
const fetchProductCatSuccess = (productCat) => ({type: FETCH_PRODUCTCAT_SUCCESS, productCat});


export const fetchProducts = () => {
    return dispatch => {
        return axios.get('/products').then(response => {
            dispatch(fetchProductsSuccess(response.data))
        })
        .catch(error => {
            dispatch(fetchProductsError(error))
        })
    }
};

export const fetchProduct = catId => {
    return dispatch => {
        return axios.get('/products?categories=' + catId).then(response => {
            dispatch(fetchProductCatSuccess(response.data));
        })
    };
};

